from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoForm


# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_detail,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            add_todo = form.save(False)
            add_todo.name = request.user
            add_todo.save()
            return redirect("todo_list_detail")
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    edit_todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=edit_todo)
        if form.is_valid():
            edit_todo = form.save()
            return redirect("todo_list_detail", id=edit_todo.id)
    else:
        form = TodoForm(instance=edit_todo)
    context = {
        "form": form,
    }
    return render(request, "todos/update.html", context)
